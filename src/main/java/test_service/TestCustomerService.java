/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author moomoo
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        
        for(Customer c : cs.getCustomers()) {
            System.out.println(c);
        } System.out.println(cs.getByTel("0888888888"));
        Customer cus1 = new Customer("oak", "0639307751");
        cs.addNew(cus1);
        for(Customer c : cs.getCustomers()) {
            System.out.println(c);
        }
        Customer delCus = cs.getByTel("0639307751");
        delCus.setTel("0639307750");
        cs.update(delCus);
        System.out.println("After Updated");
        for(Customer c : cs.getCustomers()) {
            System.out.println(c); }
        cs.delete(delCus);
        for(Customer c : cs.getCustomers()) {
            System.out.println(c);
        }
    }
}
